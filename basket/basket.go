// Basket creates baskets for Fano 3-folds for use in the fano3 database.

/*

Create baskets for Fano 3-folds for use in the fano3 database.
These are NOT generic sing/basket functions.

Baskets are slices of singularities.

*/

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package basket

import (
	"bitbucket.org/pcas/fano3/singularity"
	"bitbucket.org/pcas/math/errors"
	"database/sql/driver"
	errorsstd "errors"
	"sort"
)

// Points is a basket of point singularities (a slice of points)
type Points struct {
	b singularity.PointSlice
}

//////////////////////////////////////////////////////////////////////

// New creates a basket from a point slice WHICH IT SORTS
func New(b singularity.PointSlice) *Points {
	c := make(singularity.PointSlice, b.Len())
	copy(c, b)
	sort.Sort(c)
	return &Points{
		b: c,
	}
}

// Entry returns the ith entry of basket B
func (B *Points) Entry(i int) *singularity.Point {
	return B.b[i]
}

// Remove the ith entry of basket B WHICH IS ORDERED
func (B *Points) Remove(i int) *Points {
	c := make(singularity.PointSlice, 0, B.Len()-1)
	c = append(c, B.b[0:i]...)
	c = append(c, B.b[i+1:len(B.b)]...)
	return &Points{b: c}
}

// Include adds the given point to the basket
func (B *Points) Include(p ...*singularity.Point) *Points {
	size := B.Len()
	c := make(singularity.PointSlice, size, size+len(p))
	copy(c, B.b)
	c = append(c, p...)
	sort.Sort(c)
	return &Points{
		b: c,
	}
}

// String represents a basket as a string (as a slice of int slices)
func (B *Points) String() string {
	if B == nil {
		return "[]"
	}
	return B.b.String()
}

// Fano3Basket is a standard basket constructor for the fano3 db. The input is a pair of parallel slices R = [r1,r2,...] and A = [a1,a2,...]. Return the basket [[r1,a1],[r2,a2],...].
func Fano3Basket(R []int, A []int) (*Points, error) {
	if len(R) != len(A) {
		return nil, errors.SliceWrongLength.New()
	}
	b := make(singularity.PointSlice, 0, len(R))
	for idx, r := range R {
		p, err := singularity.New(r, A[idx])
		if err != nil {
			return nil, errorsstd.New("Parallel sequences do not represent singularities")
		}
		b = append(b, p)
	}
	return New(b), nil
}

// Len returns the length of the basket B
func (B *Points) Len() int {
	if B == nil {
		return 0
	}
	return B.b.Len()
}

//////////////////////////////////////////////////////////////////////
// Functions to assist database translation

// Value
func (B *Points) Value() (driver.Value, error) {
	return B.String(), nil
}

// Scan
func (B *Points) Scan(src interface{}) error {
	if B == nil {
		return errorsstd.New("Illegal nil argument")
	}
	s, ok := src.(string)
	if !ok {
		return errorsstd.New("Scan only accepts a string")
	}
	b, err := singularity.PointSliceFromString(s)
	if err != nil {
		return err
	}
	sort.Sort(b)
	B.b = b
	return nil
}
