// Basket_test provides tests for the basket package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package basket

import (
	"bitbucket.org/pcas/fano3/singularity"
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

func TestIntSlice(t *testing.T) {
	assert := assert.New(t)
	p1, _ := singularity.New(2, 1)
	p2, _ := singularity.New(7, 2)
	b := singularity.PointSlice{p1, p1, p2, p1}
	B := New(b)
	str := B.String()
	assert.Equal("[[2,1],[2,1],[2,1],[7,2]]", str)
}
