// Db provides functions to read and write fano data out to database.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package db

import (
	"bitbucket.org/pcas/fano3/fano3fold"
	"bitbucket.org/pcas/fano3/fanoutil"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial/integerpolynomial"
	"bitbucket.org/pcas/math/vector/integervector"
	"database/sql"
	"fmt"
	"strings"
)

//////////////////////////////////////////////////////////////////////

func CreateTable(db *sql.DB, tablename string, numHilbs int) {

	// Delete the table if it already exists
	db.Exec("DROP TABLE IF EXISTS " + tablename)
	// Create the table
	createsql := "CREATE TABLE " + tablename + "(grdbid INT, g INT, B STRING, W STRING, codim INT, deg RAT, Pnum STRING"
	for i := 1; i <= numHilbs; i++ {
		createsql += fmt.Sprintf(", hA%d INT", i)
	}
	createsql += ")"
	if _, err := db.Exec(createsql); err != nil {
		panic(err)
	}
	if _, err := db.Exec("CREATE UNIQUE INDEX grdbid_index ON " + tablename + "(grdbid ASC)"); err != nil {
		panic(err)
	}
	if _, err := db.Exec("CREATE INDEX g_index ON " + tablename + "(g ASC)"); err != nil {
		panic(err)
	}
	if _, err := db.Exec("CREATE INDEX B_index ON " + tablename + "(B ASC)"); err != nil {
		panic(err)
	}
	if _, err := db.Exec("CREATE INDEX hA_index ON " + tablename + " (hA1, hA2, hA3)"); err != nil {
		panic(err)
	}
}

func AssignIDs(db *sql.DB, tablename string, numHilbs int) {
	// Finally sort according Hilbert coefficients
	updatestmt, err := db.Prepare("UPDATE " + tablename + " SET grdbid=? WHERE grdbid=?")
	if err != nil {
		panic(err)
	}
	defer updatestmt.Close()
	createsql := "SELECT grdbid FROM " + tablename + " ORDER BY "
	for i := 1; i <= numHilbs; i++ {
		createsql += fmt.Sprintf(" hA%d,", i)
	}
	createsql = strings.TrimRight(createsql, ",")
	rows, err := db.Query(createsql)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	grdbids := make([]int, 0, 40000)
	for rows.Next() {
		var grdbid int
		if err := rows.Scan(&grdbid); err != nil {
			panic(err)
		}
		grdbids = append(grdbids, grdbid)
	}
	for i, g := range grdbids {
		_, err := updatestmt.Exec(i+1, g)
		if err != nil {
			panic(err)
		}
	}
}

// Change the weights of Fano number id to newW - update codim and numerator
// fields to match.
func UpdateID(db *sql.DB, tablename string, id int, newW []int) {
	Wstr := fanoutil.IntSliceToString(newW)
	createsql := "UPDATE " + tablename + " SET W = ?, codim = ? WHERE grdbid = ?"
	_, err := db.Exec(createsql, Wstr, len(newW)-4, id)
	if err != nil {
		panic(err)
	}
	// THINK: still have to do numerator
}

//////////////////////////////////////////////////////////////////////
//		various workers
//////////////////////////////////////////////////////////////////////

// getNumCoeffs retrieves the coefficients of Pnum
func getNumCoeffs(Pnum *integerpolynomial.Element) []*integer.Element {
	M := integerpolynomial.ExponentMonoid(Pnum.Parent().(*integerpolynomial.Parent))
	d, _ := Pnum.Degree()
	dint, _ := d.EntryOrPanic(0).Int64()
	numcoeffs := make([]*integer.Element, 0, int(dint)+1)
	for i := int64(0); i <= dint; i++ {
		m, _ := integervector.FromInt64Slice(M, []int64{i})
		coeff, _ := Pnum.Coefficient(m)
		numcoeffs = append(numcoeffs, coeff)
	}
	return numcoeffs
}

// OutputResults writes the Fano 3-folds from resC to tablename in database db (using numHilbs coefficients of the Hilbert series
func OutputResults(db *sql.DB, tablename string, numHilbs int, resC <-chan *fano3fold.Variety, doneC chan<- struct{}) {
	defer close(doneC)

	// Prepare the INSERT statement
	createsql := "INSERT INTO " + tablename + "(grdbid,g,B,W,codim,deg,Pnum"
	valuessql := ") VALUES (?,?,?,?,?,?,?"
	for i := 1; i <= numHilbs; i++ {
		createsql += fmt.Sprintf(", hA%d", i)
		valuessql += ", ?"
	}
	createsql += valuessql + ")"
	stmt, err := db.Prepare(createsql)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	// Loop over the results and prepare their data; then add to db
	grdbid := -1
	for X := range resC {
		// Need to store g, B, W, codim, deg, Pnum's coeffs (which we get next)
		numcoeffs := getNumCoeffs(X.HilbertNumerator())

		// Write this Fano to the database
		Wstr := fanoutil.IntSliceToString(X.Weights())
		vals := []interface{}{
			grdbid, X.Genus(), X.Basket(), Wstr, X.Codimension(), X.Degree().String(), fanoutil.IntegerSliceToString(numcoeffs),
		}
		coeffstr := make([]string, 0, numHilbs)
		cs := X.HilbertCoefficients(numHilbs + 1)
		for _, c := range cs[1:] {
			coeffstr = append(coeffstr, c.String())
		}
		coeffvals := make([]interface{}, len(coeffstr))
		for i, v := range coeffstr {
			coeffvals[i] = v
		}
		vals = append(vals, coeffvals...)
		_, err := stmt.Exec(vals...)
		if err != nil {
			panic(err)
		}
		grdbid--
	}
}
