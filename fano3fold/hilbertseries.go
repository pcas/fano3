// Hilbertseries TODO write descriptive comment here.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fano3fold

import (
	"bitbucket.org/pcas/fano3/basket"
	"bitbucket.org/pcas/fano3/fanoutil"
	"bitbucket.org/pcas/fano3/singularity"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial/integerpolynomial"
	"bitbucket.org/pcas/math/polynomial/rationalpolynomial"
	"bitbucket.org/pcas/math/rational"
)

const Precision = 100

//////////////////////////////////////////////////////////////////////

// Kc2 returns the rational number -K_X*c2(X) of an index 1 Fano 3-fold X with basket B = [ [r,a], ... ]
func Kc2(B *basket.Points) *rational.Element {
	size := B.Len()
	if size == 0 {
		return rational.FromInt(2)
	}
	sumpart := rational.FromInt(0)
	r := rational.FromInt(0)
	for i := 0; i < size; i++ {
		p := B.Entry(i)
		r = rational.FromInt(p.Index())
		r2, _ := rational.Power(r, integer.FromInt(2))
		extra := rational.Add(r2, rational.FromInt(-1))
		extra, _ = rational.Divide(extra, r)
		sumpart = rational.Add(sumpart, extra)
	}
	return rational.Add(rational.FromInt(24), rational.Negate(sumpart))
}

//////////////////////////////////////////////////////////////////////

// Degree returns the degree -K_X^3 of an index 1 Fano 3-fold X with genus g and basket B = [ [r,a], ... ]
func Degree(g int, B *basket.Points) *rational.Element {
	size := B.Len()
	deg := rational.FromInt(2*g - 2)
	for i := 0; i < size; i++ {
		p := B.Entry(i)
		r := integer.FromInt(p.Index())
		a := integer.FromInt(p.Polarisation())
		b, _ := a.InverseMod(r)
		extranum := integer.Multiply(b, integer.Subtract(r, b))
		extra, _ := rational.FromIntegerPair(extranum, r)
		deg = rational.Add(deg, extra)
	}
	return deg
}

/*
// The correction to RR for nA (n taken modulo r) coming from the singularity p = [r,a], that is 1/r(1,a,r-a)
func contribution(p []int, n *integer.Element) (*rational.Element, error) {
	if len(p) != 2 {
		error("p must be of the form [r,a]")
	}
	r := integer.FromInt(p[0])
	if (n.Mod(r)) == 0 {
		return rational.FromInt(0), nil
	}
	a := integer.FromInt(p[1])
}
*/

// periodicPart is an internal function computing the periodic part of [ABR] (4.6.2)
func periodicPart(p *singularity.Point, R *rationalpolynomial.Parent) *rationalpolynomial.Element {
	r := integer.FromInt(p.Index())
	a := integer.FromInt(p.Polarisation())
	b, _ := a.InverseMod(r)
	per := rationalpolynomial.Zero(R)
	for i := 1; i < p.Index(); i++ {
		ii := integer.FromInt(i)
		bi := integer.Multiply(b, ii)
		bibar, _ := bi.Mod(r)
		coeffnum := integer.Multiply(bibar, integer.Subtract(r, bibar))
		coeff, _ := rational.FromIntegerPair(coeffnum, integer.Multiply(integer.FromInt(2), r))
		mono, _ := rationalpolynomial.FromRationalSlice(R, int64(i), 0, []*rational.Element{coeff})
		per, _ = rationalpolynomial.Add(per, mono)
	}
	return per
}

// Hilbert series returns the Hilbert series of an index 1 Fano 3-fold with genus g and basket B = [ [r,a], ... ]; the degree is returned as a second value.
func HilbertSeries(g int, B *basket.Points, precision int) (*integerpolynomial.Element, *rational.Element) {
	if g < -2 {
		panic("The genus g must be an integer >= -2")
	}
	R := rationalpolynomial.NewUnivariateRing()
	one := rationalpolynomial.One(R)
	t, _ := rationalpolynomial.FromIntSlice(R, 1, 0, []int{1})
	oneplust, _ := rationalpolynomial.Add(one, t)
	oneminustinv := rationalpolynomial.One(R)
	for e := 1; e < precision; e++ {
		mono, _ := rationalpolynomial.FromIntSlice(R, int64(e), 0, []int{1})
		oneminustinv, _ = rationalpolynomial.Add(oneminustinv, mono)
	}
	// Compute the first term I
	omt2, _ := oneminustinv.Power(integer.FromInt(2))
	omt2 = fanoutil.TrimRationalPolynomial(omt2, precision)
	I, _ := rationalpolynomial.Multiply(oneplust, omt2)
	I = fanoutil.TrimRationalPolynomial(I, precision)
	// Compute II
	deg := Degree(g, B)
	half, _ := rational.FromIntPair(1, 2)
	deg2 := rational.Multiply(deg, half)
	omt4, _ := omt2.Power(integer.FromInt(2))
	omt4 = fanoutil.TrimRationalPolynomial(omt4, precision)
	IInum, _ := rationalpolynomial.Multiply(t, oneplust)
	II, _ := rationalpolynomial.Multiply(IInum, omt4)
	II = fanoutil.TrimRationalPolynomial(II, precision)
	II, _ = II.ScalarMultiplyByCoefficient(deg2)
	// Compute III
	III := rationalpolynomial.Zero(R)
	size := B.Len()
	for i := 0; i < size; i++ {
		p := B.Entry(i)
		r := p.Index()
		omtrinv := rationalpolynomial.One(R) // 1 / (1 - t^r)
		rel_prec := 1 + precision/r
		for e := 1; e < rel_prec; e++ {
			mono, _ := rationalpolynomial.FromIntSlice(R, int64(e*r), 0, []int{1})
			omtrinv, _ = rationalpolynomial.Add(omtrinv, mono)
		}
		factor, _ := rationalpolynomial.Multiply(oneminustinv, omtrinv)
		// periodic part
		periodic := periodicPart(p, R)
		IIInext, _ := rationalpolynomial.Multiply(factor, periodic)
		IIInext = rationalpolynomial.Negate(IIInext)
		III, _ = rationalpolynomial.Add(III, IIInext)
	}
	III = fanoutil.TrimRationalPolynomial(III, precision)
	h, _ := rationalpolynomial.Add(I, II)
	h, _ = rationalpolynomial.Add(h, III)
	hprec := fanoutil.TrimRationalPolynomial(h, precision)
	hint, err := fanoutil.ToIntegralPolynomial(hprec)
	if err != nil {
		panic("RR calculation not integral")
	}
	return hint, deg
}
