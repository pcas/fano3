// Fano3folds TODO write descriptive comment here.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fano3fold

import (
	"bitbucket.org/pcas/fano3/basket"
	"bitbucket.org/pcas/fano3/fanoutil"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial/integerpolynomial"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/vector/integervector"
	"bitbucket.org/pcastools/fatal"
	"errors"
	"fmt"
)

// Variety contains the numerical data that represents a Fano 3-fold.
type Variety struct {
	g       int
	basket  *basket.Points
	weights []int
	hilb    *integerpolynomial.Element
	pnum    *integerpolynomial.Element
	deg     *rational.Element
	kc2     *rational.Element
}

// New creates a Fano 3-fold with genus g and basket B
func New(g int, B *basket.Points) (*Variety, error) {
	if g < -2 {
		return nil, errors.New("Genus g must be at least -2")
	}
	return &Variety{
		g:      g,
		basket: B,
	}, nil
}

// Genus returns the genus g of X
func (X *Variety) Genus() int {
	return X.g
}

// Basket returns the basket of X
func (X *Variety) Basket() *basket.Points {
	return X.basket
}

// Codimension returns the codimension of X
func (X *Variety) Codimension() int {
	if X.weights == nil {
		return -5
	}
	return len(X.weights) - 4
}

// Weights returns the weights of X if assigned
func (X *Variety) Weights() []int {
	if X.weights == nil {
		return nil
	}
	W := make([]int, len(X.weights))
	copy(W, X.weights)
	return W
}

// HilbertSeries returns the Hilbert series of X (currently as poly which is leading portion of power series).
func (X *Variety) HilbertSeries() *integerpolynomial.Element {
	if X.hilb != nil {
		return X.hilb
	}
	h, deg := HilbertSeries(X.Genus(), X.Basket(), Precision)
	if X.deg != nil && !deg.IsEqualTo(X.deg) {
		panic("Degree has been set incorrectly")
	}
	X.hilb = h
	X.deg = deg
	return h
}

// HilbertCoefficients returns the coefficients Hilbert series of X, starting with the constant coefficient and ending with the coefficient of x^(n-1)
func (X *Variety) HilbertCoefficients(n int) []*integer.Element {
	if n >= Precision {
		panic("Not enough precision in the Hilbert series")
	}
	h := X.HilbertSeries()
	M := integerpolynomial.ExponentMonoid(h.Parent().(*integerpolynomial.Parent))
	coeffs := make([]*integer.Element, 0, n)
	for i := 0; i < n; i++ {
		mi, _ := integervector.FromIntSlice(M, []int{i})
		c, _ := h.Coefficient(mi)
		coeffs = append(coeffs, c)
	}
	return coeffs
}

// HilbertNumerator returns the numerator of the Hilbert series of X w.r.t the weights.
func (X *Variety) HilbertNumerator() *integerpolynomial.Element {
	if X.pnum != nil {
		return X.pnum
	} else if X.weights == nil {
		return nil
	}
	h := X.HilbertSeries()
	wts := X.weights
	// make a slice of 1-t^a factors
	R := integerpolynomial.DefaultUnivariateRing()
	for _, w := range wts {
		f, err := integerpolynomial.FromParallelIntSlices(R, []int{1, -1}, []int{0, w})
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		h, err = integerpolynomial.Multiply(h, f)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
	}
	h = fanoutil.TrimIntegerPolynomial(h, Precision)
	X.pnum = h
	return h
}

// Degree returns the degree of X
func (X *Variety) Degree() *rational.Element {
	if X.deg == nil {
		X.deg = Degree(X.Genus(), X.Basket())
	}
	return X.deg
}

// Kc2 returns the rational number K_X * c_2(X)
func (X *Variety) Kc2() *rational.Element {
	if X.kc2 == nil {
		X.kc2 = Kc2(X.Basket())
	}
	return X.kc2
}

// AssignWeights sets the weights to W (and clears the Hilbert numerator)
func (X *Variety) AssignWeights(W []int) {
	myW := make([]int, len(W))
	copy(myW, W)
	X.weights = myW
	X.pnum = nil
}

// String returns Fano 3-fold X as a string
func (X *Variety) String() string {
	str := fmt.Sprintf("Fano 3-fold in codimension %d with data\n", X.Codimension())
	str += fmt.Sprintf("\tWeights: %s\n\tBasket: %s\n",
		fanoutil.IntSliceToString(X.weights), X.Basket().String())
	str += fmt.Sprintf("\tDegree: -K^3 = %s\tK*c2(X) = %s\n",
		X.Degree().String(), X.Kc2().String())
	str += fmt.Sprintf("\tNumerator: %s", X.HilbertNumerator().String())
	return str

}
