// Fano3fold_test provides tests for the fano3fold package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fano3fold

import (
	"bitbucket.org/pcas/fano3/basket"
	"bitbucket.org/pcas/math/testing/assert"
	"fmt"
	"testing"
)

func TestFano(t *testing.T) {
	assert := assert.New(t)
	R := []int{2, 2, 5}
	A := []int{1, 1, 2}
	B, err := basket.Fano3Basket(R, A)
	assert.NoError(err)
	X, err := New(0, B)
	assert.NoError(err)
	fmt.Println(X)
	X.AssignWeights([]int{1, 1, 2, 5, 8})
	fmt.Println(X)
}
