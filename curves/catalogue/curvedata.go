// Curvedata computes the RR contributions of subcanonical curves.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial/integerpolynomial"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/vector/integervector"
	"bitbucket.org/pcastools/mathutil"
	stderrors "errors"
	"fmt"
)

const precision = 100
const inputFile = "in.txt"

//////////////////////////////////////////////////////////////////////

// Return polynomial f with all terms of degree >= 100 removed
func trimPolynomial(f *integerpolynomial.Element, d int) *integerpolynomial.Element {
	if f.IsZero() {
		return f
	}
	R := f.Parent().(*integerpolynomial.Parent)
	g := integerpolynomial.Zero(R)
	for _, t := range f.Terms() {
		degt, err := t.Degree()
		if err != nil {
			panic(err)
		}
		if degt.EntryOrPanic(0).IsLessThanInt64(int64(d)) {
			g, _ = integerpolynomial.Add(g, t)
		}
	}
	return g
}

// Pnum is a poly of the form a0 + a1 * t + a2 * t^2 + ...
// Return ad, d where d > 0 is the smallest degree for which ad is not 0.
// If Pnum = a0, then return nil,nil. Otherwise error.
func FirstNonzeroCoefficient(Pnum *integerpolynomial.Element) (*integer.Element, *integervector.Element, error) {
	if Pnum.IsZero() {
		return nil, nil, errors.IllegalZeroArg.New()
	}
	a0 := Pnum.ConstantCoefficient()
	if !(a0.IsOne()) {
		return nil, nil, stderrors.New("Constant coefficient should be 1")
	}
	R := Pnum.Parent().(*integerpolynomial.Parent)
	a0const, _ := integerpolynomial.FromIntegerSlice(R, 0, 1, []*integer.Element{a0})
	f, _ := integerpolynomial.Subtract(Pnum, a0const)
	if f.IsZero() {
		return integer.Zero(), integervector.Zero(integerpolynomial.ExponentMonoid(R)), nil
	} else {
		val, _ := f.Valuation()
		coeff, _ := f.Coefficient(val)
		return coeff, val, nil
	}
}

// a is integer, e is (integer) - return (1 - t^(e))^a.
func makeFactor(a *integer.Element, e *integervector.Element) {
}

// Pnum is a hilbert numerator.
// Return an sequence of integers for which the first nonconstant coefficient of
//			Pnum / &*[ 1 - t^w : w in W ]
// is negative (or else is nonexistent to precision of 100 terms).
func FindFirstGenerators(Pnum *integerpolynomial.Element) ([]*integervector.Element, *integerpolynomial.Element, error) {
	R := Pnum.Parent().(*integerpolynomial.Parent)
	// Calculate Hilbert series as polynomial to 100 terms
	P := Pnum
	//factor := R.Zero()
	//maxj := 0
	a, e, _ := FirstNonzeroCoefficient(P)
	W := []*integervector.Element{}
	for a.IsPositive() {
		W = append(W, e)
		one, _ := integerpolynomial.ToTerm(R, integer.FromInt(1), integervector.Zero(integerpolynomial.ExponentMonoid(R)))
		mono, _ := integerpolynomial.ToMonomial(R, e)
		factor, _ := integerpolynomial.Subtract(one, mono)
		P, _ = integerpolynomial.Multiply(factor, P)
		a, e, _ = FirstNonzeroCoefficient(P)
	}
	Ptrim := trimPolynomial(P, precision)
	return W, Ptrim, nil
}

func integerBelow(q *rational.Element) *integer.Element {
	if q.IsIntegral() {
		return q.Floor().Decrement()
	}
	return q.Floor()
}

func plusDualEff(Q []*integer.Element, g int, d int, half int, top int) (bool, []*integer.Element) {
	n := len(Q) - 1
	fmt.Println("HERE 6", top, n, Q)
	qtop, err := Q[top-n].Int64()
	fmt.Println("HERE 7")
	if err != nil {
		panic(err)
	}
	qnnew, ok := mathutil.AddInt64(int64(d*n+1-g), qtop)
	if !ok {
		panic(errors.OutOfRange.New())
	}
	Q[n] = integer.FromInt64(qnnew)
	if integer.Subtract(Q[n], Q[n-1]).IsGreaterThan(integer.FromInt(d)) {
		panic("RR error")
	}

	for i := n + 1; i <= top; i++ {
		Q = append(Q, integer.Add(integer.FromInt(d*i+1-g), Q[top-i]))
	}
	return true, Q
}

func EffectivePossibilities(g int, d int) ([][]*integer.Element, error) {
	fmt.Println("---------------")
	euler := 2*g - 2
	if !(euler%d == 0) {
		panic("d must divide 2g-2")
	}
	top := euler / d
	half := 0
	if top%2 == 0 {
		half = top / 2
	} else {
		half = (top - 1) / 2
	}
	fmt.Println("g,d,euler,top,half =", g, d, euler, top, half)

	prehyper := make([]*rational.Element, 0, half+1)
	for i := 0; i <= half; i++ {
		id2, _ := rational.FromIntPair(i*d, 2)
		prehyper = append(prehyper, id2.Increment())
	}
	fmt.Println("prehyper =", prehyper)

	hyper := make([]*integer.Element, 0, half+1)
	for _, val := range prehyper {
		hyper = append(hyper, val.Floor())
	}
	fmt.Println("hyper =", hyper)

	maxnonhyper := make([]*integer.Element, 0, half+1)
	for _, val := range prehyper {
		maxnonhyper = append(maxnonhyper, integerBelow(val))
	}
	maxnonhyper[0] = integer.One()
	fmt.Println("maxnonhyper =", maxnonhyper)

	seqs := [][]*integer.Element{{integer.One()}}
	for i := 1; i <= half; i++ {
		seqs0 := make([][]*integer.Element, 0)
		for _, Q := range seqs {
			n := Q[len(Q)-1]
			n64, err := n.Int64()
			if err != nil {
				panic(err)
			}
			nd, ok := mathutil.AddInt64(n64, int64(d))
			if !ok {
				panic(errors.OutOfRange.New())
			}
			q1, err := maxnonhyper[len(Q)].Int64()
			if err != nil {
				panic(err)
			}
			m := q1
			if nd < q1 {
				m = nd
			}
			for j := n64; j <= m; j++ {
				Q1 := append(Q, integer.FromInt64(j))
				seqs0 = append(seqs0, Q1)
			}
		}
		seqs = seqs0
	}
	fmt.Println("seqs =", seqs)

	seqs1 := [][]*integer.Element{}
	for _, Q := range seqs {
		ok, Q1 := plusDualEff(Q, g, d, half, top)
		if !ok {
			continue
		}
		seqs1 = append(seqs1, Q1)
	}

	_, hyper1 := plusDualEff(hyper, g, d, half, top)
	seqs1 = append(seqs1, hyper1)
	fmt.Println("seqs1 =", seqs1)

	return seqs1, nil
}

func HilbertSeries(g int, d int, V []*integer.Element) (*integerpolynomial.Element, error) {
	R := integerpolynomial.NewUnivariateRing()
	P := integerpolynomial.Zero(R)
	curridx := 0
	for idx, val := range V {
		expt, _ := integervector.FromIntSlice(integerpolynomial.ExponentMonoid(R), []int{idx})
		nextterm, _ := integerpolynomial.ToTerm(R, val, expt)
		P, _ = integerpolynomial.Add(P, nextterm)
		curridx = idx
	}
	coeff := g - 1 + d
	for curridx < precision {
		curridx += 1
		expt, _ := integervector.FromIntSlice(integerpolynomial.ExponentMonoid(R), []int{curridx})
		nextterm, _ := integerpolynomial.ToTerm(R, integer.FromInt(coeff), expt)
		P, _ = integerpolynomial.Add(P, nextterm)
		coeff += d
	}
	return P, nil
}

//////////////////////////////////////////////////////////////////////
//	testing area and main
//////////////////////////////////////////////////////////////////////

func main() {
	for g := 3; g < 6; g++ {
		euler := 2*g - 2
		for d := 1; d <= euler; d++ {
			fmt.Println("g,d,rem =", g, d, euler%d)
			if !(euler%d == 0) {
				fmt.Println("HERE")
				continue
			}
			Ws, _ := EffectivePossibilities(g, d)
			fmt.Println("#Ws =", len(Ws))
		}
	}
}
