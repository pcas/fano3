// Fanoutil_test provides tests for the fanoutil package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fanoutil

import (
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

func TestIntSlice(t *testing.T) {
	assert := assert.New(t)
	s := []int{1, 5, 11, -73}
	str := IntSliceToString(s)
	assert.Equal("[1,5,11,-73]", str)
}
