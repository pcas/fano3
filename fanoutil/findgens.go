// Findgens provides functions to analyse Hilbert series.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fanoutil

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial/integerpolynomial"
	"bitbucket.org/pcas/math/vector/integervector"
	stderrors "errors"
)

//////////////////////////////////////////////////////////////////////

// Pnum is a poly of the form a0 + a1 * t + a2 * t^2 + ...
// Return ad, d where d > 0 is the smallest degree for which ad is not 0.
// If Pnum = a0, then return nil,nil. Otherwise error.
func FirstNonzeroCoefficient(Pnum *integerpolynomial.Element) (*integer.Element, *integervector.Element, error) {
	if Pnum.IsZero() {
		return nil, nil, errors.IllegalZeroArg.New()
	}
	a0 := Pnum.ConstantCoefficient()
	if !(a0.IsOne()) {
		return nil, nil, stderrors.New("Constant coefficient should be 1")
	}
	R := Pnum.Parent().(*integerpolynomial.Parent)
	a0const, _ := integerpolynomial.FromIntegerSlice(R, 0, 1, []*integer.Element{a0})
	// f, _ := integerpolynomial.Subtract(Pnum, a0const)
	// THINK: replace with Subtract
	atemp := integerpolynomial.Negate(a0const)
	f, _ := integerpolynomial.Add(Pnum, atemp)
	if f.IsZero() {
		return integer.Zero(), integervector.Zero(integerpolynomial.ExponentMonoid(R)), nil
	} else {
		val, _ := f.Valuation()
		coeff, _ := f.Coefficient(val)
		return coeff, val, nil
	}
}

// Pnum is a hilbert numerator.
// Return an sequence of integers for which the first nonconstant coefficient of
//			Pnum / &*[ 1 - t^w : w in W ]
// is negative (or else is nonexistent to precision of 100 terms).
func FindFirstGenerators(Pnum *integerpolynomial.Element, precision int) ([]int, *integerpolynomial.Element, error) {
	R := Pnum.Parent().(*integerpolynomial.Parent)
	// Calculate Hilbert series as polynomial to 100 terms
	P := Pnum
	//factor := R.Zero()
	//maxj := 0
	a, e, _ := FirstNonzeroCoefficient(P)
	W := []*integervector.Element{}
	for a.IsPositive() {
		aa, err := a.Int64()
		if err != nil {
			panic(err)
		}
		W = append(W, integervector.NCopiesToSlice(e, int(aa))...)
		one, _ := integerpolynomial.ToTerm(R, integer.FromInt(1), integervector.Zero(integerpolynomial.ExponentMonoid(R)))
		mono, _ := integerpolynomial.ToMonomial(R, e)
		factor0, _ := integerpolynomial.Subtract(one, mono)
		factor, _ := factor0.Power(a)
		P, _ = integerpolynomial.Multiply(factor, P)
		a, e, _ = FirstNonzeroCoefficient(P)
	}
	Ptrim := TrimIntegerPolynomial(P, precision)
	Wint := make([]int, 0, len(W))
	for _, e := range W {
		w, err := e.EntryOrPanic(0).Int64()
		if err != nil {
			return nil, nil, err
		}
		Wint = append(Wint, int(w))
	}
	return Wint, Ptrim, nil
}
