// Fanoutil TODO write descriptive comment here.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fanoutil

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcastools/slice"
	"strconv"
	"strings"
)

// IntSliceToString represents a slice of ints as a string
func IntSliceToString(S []int) string {
	strs := make([]string, 0, len(S))
	for _, x := range S {
		strs = append(strs, strconv.Itoa(x))
	}
	return "[" + strings.Join(strs, ",") + "]"
}

// SliceOfIntSliceToString represents a slice of int slices as a string
func SliceOfIntSliceToString(S [][]int) string {
	strs := make([]string, 0, len(S))
	for _, x := range S {
		strs = append(strs, IntSliceToString(x))
	}
	return "[" + strings.Join(strs, ",") + "]"
}

// IntegerSliceToString represents a slice of integers as a string
func IntegerSliceToString(S []*integer.Element) string {
	strs := make([]string, 0, len(S))
	for _, x := range S {
		strs = append(strs, x.String())
	}
	return "[" + strings.Join(strs, ",") + "]"
}

// IntegerSliceToSingularity rewrites [r,a] as string 1/r(1,a,r-a)
func IntegerSliceToSingularity(S []int) string {
	if len(S) == 0 {
		return ""
	} else if len(S) != 2 {
		panic("Argument was not an [r,a] pair")
	}
	ra := S[0] - S[1]
	return "1/" + strconv.Itoa(S[0]) + "(1," + strconv.Itoa(S[1]) + "," + strconv.Itoa(ra) + ")"
}

// SliceOfIntegerSliceToString represents a slice of integer slices as a string
func SliceOfIntegerSliceToString(S [][]*integer.Element) string {
	strs := make([]string, 0, len(S))
	for _, x := range S {
		strs = append(strs, IntegerSliceToString(x))
	}
	return "[" + strings.Join(strs, ",") + "]"
}

// SliceOfIntSliceFromString creates the same from string
func SliceOfIntSliceFromString(str string) ([][]int, error) {
	if str == "[]" {
		return [][]int{}, nil
	}
	SS := strings.Split(str[2:len(str)-2], "],[")
	T := make([][]int, 0, len(SS))
	for _, s := range SS {
		ss, err := slice.IntSliceFromString(s)
		if err != nil {
			return nil, err
		}
		T = append(T, ss)
	}
	return T, nil
}
