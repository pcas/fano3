// Pstools provides tools to simulate power series.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fanoutil

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial/integerpolynomial"
	"bitbucket.org/pcas/math/polynomial/rationalpolynomial"
)

//////////////////////////////////////////////////////////////////////

// TrimIntegerPolynomial returns polynomial f with all terms of degree >= d removed
func TrimIntegerPolynomial(f *integerpolynomial.Element, d int) *integerpolynomial.Element {
	if f.IsZero() {
		return f
	}
	R := f.Parent().(*integerpolynomial.Parent)
	g := integerpolynomial.Zero(R)
	for _, t := range f.Terms() {
		degt, err := t.Degree()
		if err != nil {
			panic(err)
		}
		if degt.EntryOrPanic(0).IsLessThanInt64(int64(d)) {
			g, _ = integerpolynomial.Add(g, t)
		}
	}
	return g
}

// TrimRationalPolynomial returns polynomial f with all terms of degree >= d removed
func TrimRationalPolynomial(f *rationalpolynomial.Element, d int) *rationalpolynomial.Element {
	if f.IsZero() {
		return f
	}
	R := f.Parent().(*rationalpolynomial.Parent)
	g := rationalpolynomial.Zero(R)
	for _, t := range f.Terms() {
		degt, err := t.Degree()
		if err != nil {
			panic(err)
		}
		if degt.EntryOrPanic(0).IsLessThanInt64(int64(d)) {
			g, _ = rationalpolynomial.Add(g, t)
		}
	}
	return g
}

// ToIntegralPolynomial returns the rational polynomial f as an integral polynomial, if all its coefficients are in fact integers; otherwise an error.
func ToIntegralPolynomial(f *rationalpolynomial.Element) (*integerpolynomial.Element, error) {
	cs, es := f.CoefficientsAndExponents()
	csint := []*integer.Element{}
	for _, c := range cs {
		if !c.IsIntegral() {
			return nil, errors.RationalNotAnInteger.New()
		}
		cint, _ := c.ToInteger()
		csint = append(csint, cint)
	}
	R := integerpolynomial.DefaultUnivariateRing()
	fint, _ := integerpolynomial.FromParallelSlices(R, csint, es)
	return fint, nil
}
