module bitbucket.org/pcas/fano3

go 1.13

require (
	bitbucket.org/pcas/math v0.0.1
	bitbucket.org/pcastools/fatal v1.0.1
	bitbucket.org/pcastools/mathutil v1.0.2
	bitbucket.org/pcastools/slice v1.0.1
	github.com/mattn/go-sqlite3 v1.11.0
)
