// TODO need descriptive comment here.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/fano3/basket"
	ourdb "bitbucket.org/pcas/fano3/db"
	"bitbucket.org/pcas/fano3/fano3fold"
	"bitbucket.org/pcas/fano3/fanoutil"
	"bitbucket.org/pcas/fano3/projection"
	"bitbucket.org/pcas/fano3/singularity"
	"bitbucket.org/pcas/math/polynomial/integerpolynomial"
	"bitbucket.org/pcas/math/vector/integervector"
	"bufio"
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"io"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

const inputFile = "fullfanodata.txt"
const numHilbs = 25
const tablename = "fano3db"
const projectiontablename = "fano3proj"

// jobData is the data defining a job
type jobData struct {
	g int
	B *basket.Points
}

// shortestSlice returns the first slice in S of shortest length.
func shortestSlice(S [][]int) []int {
	s := S[0]
	for _, t := range S[1:] {
		if len(t) < len(s) {
			s = t
		}
	}
	return s
}

func isOrderedSubslice(U, V []int) bool {
	j, v := 0, V[0]
	for _, u := range U {
		for v != u {
			if v > u {
				return false
			}
			j++
			if j == len(V) {
				return false
			}
			v = V[j]
		}
	}
	return true
}

func isValidHilbertSeries(P *integerpolynomial.Element) bool {
	M := integerpolynomial.ExponentMonoid(P.Parent().(*integerpolynomial.Parent))
	e1, _ := integervector.FromInt64Slice(M, []int64{1})
	e5, _ := integervector.FromInt64Slice(M, []int64{5})
	c1, _ := P.Coefficient(e1)
	c5, _ := P.Coefficient(e5)
	return c1.IsZero() || !c5.IsZero()
}

func worker(jobC <-chan *jobData, resC chan<- *fano3fold.Variety, doneC chan<- bool) {
	// Start working through the jobs
	for job := range jobC {
		X, err := fano3fold.New(job.g, job.B)
		if err != nil {
			panic(err)
		}
		// Perform the task
		P := X.HilbertSeries()
		W, _, err := fanoutil.FindFirstGenerators(P, fano3fold.Precision)
		if err != nil {
			panic(err)
		}
		X.AssignWeights(W)

		// Feed the result down the results channel (but only if it is not one
		// of the 8 known counterexamples with hilb = 1 + t + ... + 0*t^5 +...
		if isValidHilbertSeries(P) {
			resC <- X
		}
	}
	// Signal that we've finished
	doneC <- true
}

func lookupCodim123Cases(g int, B [][]int) ([]int, bool) {
	// THINK: to do - look up the file fano3/magma/Fano3_codim123
	S := make([]string, 0, 2*len(B)+1)
	S = append(S, strconv.Itoa(g))
	for _, p := range B {
		S = append(S, strconv.Itoa(p[0]))
		S = append(S, strconv.Itoa(p[1]))
	}
	key := strings.Join(S, ",")
	Wnew, ok := lookup[key]
	return Wnew, ok
}

func main() {
	t := time.Now()
	// Create the communication channels
	jobC := make(chan *jobData, 3)
	resC := make(chan *fano3fold.Variety, 100)
	workerDoneC := make(chan bool)
	outputDoneC := make(chan struct{})

	// Open a connection to the SQL database
	db, err := sql.Open("sqlite3", "fano3output.db")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// switch false to true to rebuild the database
	if false {
		// Create the table in sqlite3
		ourdb.CreateTable(db, tablename, numHilbs)

		// Start the output writer
		go ourdb.OutputResults(db, tablename, numHilbs, resC, outputDoneC)

		//////////////////////////////////////////////////
		// Start the workers
		numWorkers := runtime.NumCPU() + 1
		for i := 0; i < numWorkers; i++ {
			go worker(jobC, resC, workerDoneC)
		}

		// Open the file
		f, err := os.Open(inputFile)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		r := bufio.NewReader(f)

		// Start reading the lines from the file
		var g int
		line, err := r.ReadString('\n')
		for err == nil {
			line = strings.TrimSpace(line)
			if len(line) == 0 {
				line, err = r.ReadString('\n')
				continue
			}
			// Analyse the line
			data := strings.Split(line, ":")
			if len(data) != 2 {
				panic("Incorrect line: " + line)
			}
			if data[0] == "g" {
				g, err = strconv.Atoi(strings.TrimSpace(data[1]))
				if err != nil {
					panic(err)
				}
			}
			if data[0] == "B" {
				//			basket := make([][]int, 0)
				Bslice := make(singularity.PointSlice, 0)
				// need to split up things like [2,1];[7,3]
				Braw := strings.TrimSpace(data[1])
				points := strings.Split(Braw, ";")
				// Beware: empty basket has points == [ emptystring ]
				if len(points) > 1 || len(points) == 1 && len(points[0]) > 0 {
					for _, p := range points {
						// trim the brackets[]
						p = strings.TrimSpace(p[1 : len(p)-1])
						pcomps := strings.Split(p, ",")
						if len(pcomps) != 2 {
							panic("Incorrect singularity: " + p)
						}
						rs := pcomps[0]
						as := pcomps[1]
						r, err := strconv.Atoi(rs)
						if err != nil {
							panic(err)
						}
						a, err := strconv.Atoi(as)
						if err != nil {
							panic(err)
						}
						//					newsing := []int{r, a}
						newsing, err := singularity.New(r, a)
						if err != nil {
							panic(err)
						}
						Bslice = append(Bslice, newsing)
					}
				}
				basket := basket.New(Bslice)
				// The file always sets B second, so now we have g and B.
				// Feed the jobs to the workers
				jobC <- &jobData{g: g, B: basket}
			}

			// Move on
			line, err = r.ReadString('\n')
		}
		// Handle any errors
		if err != io.EOF {
			panic(err)
		}

		close(jobC)

		//////////////////////////////////////////////////
		// Wait for the workers to finish
		for i := 0; i < numWorkers; i++ {
			<-workerDoneC
		}
		close(resC)
		// Wait for the output writer to finish
		<-outputDoneC
		fmt.Println(time.Since(t))

		//////////////////////////////////////////////////
		// Next, sort the Hilbert series
		ourdb.AssignIDs(db, tablename, numHilbs)

		//////////////////////////////////////////////////
		// Now build the projection table (but without proj types yet)
		projection.BuildProjectionTable(tablename, projectiontablename)
	}

	//////////////////////////////////////////////////
	// Now run through the database, figuring out weights and proj types

	for i := 1; i <= 39550; i++ {
		// Get current weights of Xi [ignore for now]
		gi, Bi, _, err := projection.GetFanoGenusBasketDegreeByID(i, db, tablename)

		// Run through projections with source i
		centres, targets, err := projection.GetProjectionCentresAndTargetWeightsBySourceID(i, db, tablename, projectiontablename)
		if err != nil {
			panic(err)
		}

		// Either recover known codim <= 3 weights (250 cases) or
		// recover weights of Xi from all projections (not Type 4)
		// First test the 250:
		Wnew, islowcod := lookupCodim123Cases(gi, Bi)
		// THINK to write previous function
		// lowcod is true if we got the answer; else need to check by unproj
		if i == 178 {
			Wnew := []int{2, 2, 3, 3, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 8, 8}
			ourdb.UpdateID(db, tablename, i, Wnew)
		} else if i == 368 {
			Wnew := []int{1, 4, 5, 6, 6, 7, 8, 9}
			ourdb.UpdateID(db, tablename, i, Wnew)
		} else if i == 290 {
			Wnew := []int{1, 5, 7, 8, 8, 9, 10, 11, 12}
			ourdb.UpdateID(db, tablename, i, Wnew)
		} else if i == 398 {
			Wnew := []int{1, 4, 5, 5, 6, 6, 7, 8, 9}
			ourdb.UpdateID(db, tablename, i, Wnew)
		} else if i == 272 {
			Wnew := []int{1, 7, 8, 9, 10, 10, 11, 12, 13, 14, 15}
			ourdb.UpdateID(db, tablename, i, Wnew)
		} else if !islowcod {
			// For each one, determine type/subtype (and write those)
			suggested_WXs := make([][]int, 0, 10)
			for i, p := range centres {
				WY := targets[i]
				ptype, _, W := projection.CentreAnalysis(p, WY)
				if ptype != 4 {
					suggested_WXs = append(suggested_WXs, W)
				}
				// THINK: write type/subtype to projection db here
			}
			if len(suggested_WXs) == 0 {
				// oh dear - let's leave X with its FindFirstGenerators guess
				// THINK: leave the weights that are in the db as they are
			} else {
				// The suggestions are slices of integers that are the
				// proposed degrees of a minimal generating set.
				// Therefore we want the shortest one - and to check
				// that it is a subslice of all the others.
				Wnew := shortestSlice(suggested_WXs)
				for _, V := range suggested_WXs {
					if !isOrderedSubslice(Wnew, V) {
						fmt.Println("i =", i)
						fmt.Println("Shortest W =", Wnew, "error V =", V)
						panic("Incompatible weights")
					}
				}
				ourdb.UpdateID(db, tablename, i, Wnew)
				// THINK: recompute Pnum and codim and write those too
			}
		} else {
			ourdb.UpdateID(db, tablename, i, Wnew)
		}
	}
}

var lookup = map[string][]int{
	"2":                                  {1, 1, 1, 1, 3},
	"3":                                  {1, 1, 1, 1, 1},
	"1,4,1":                              {1, 1, 1, 3, 4},
	"1,3,1":                              {1, 1, 1, 3, 5},
	"1,2,1":                              {1, 1, 1, 4, 6},
	"2,2,1":                              {1, 1, 1, 1, 2},
	"0,10,3":                             {1, 1, 3, 7, 10},
	"-1,3,1,4,1,4,1,8,3":                 {1, 3, 4, 5, 8},
	"-1,3,1,3,1,3,1,5,2,6,1":             {1, 3, 5, 6, 7},
	"-1,2,1,4,1,5,2,9,4":                 {1, 4, 5, 18, 27},
	"-1,2,1,2,1,2,1,5,1,7,2":             {1, 2, 5, 14, 21},
	"0,5,2,7,1":                          {1, 1, 5, 7, 13},
	"0,5,2,6,1":                          {1, 1, 5, 12, 18},
	"0,5,2,8,1":                          {1, 1, 5, 7, 8},
	"-1,5,1,14,3":                        {1, 3, 5, 11, 14},
	"0,4,1,7,3":                          {1, 1, 3, 4, 7},
	"-1,2,1,5,2,13,4":                    {1, 4, 5, 13, 22},
	"0,3,1,7,3":                          {1, 1, 3, 7, 11},
	"-1,2,1,3,1,3,1,4,1,7,3":             {1, 3, 4, 14, 21},
	"0,2,1,4,1,5,1":                      {1, 1, 4, 10, 15},
	"0,2,1,4,1,7,1":                      {1, 1, 4, 6, 7},
	"0,2,1,4,1,6,1":                      {1, 1, 4, 6, 11},
	"-1,2,1,4,1,13,3":                    {1, 3, 4, 10, 13},
	"0,2,1,7,2":                          {1, 1, 2, 5, 7},
	"0,2,1,9,4":                          {1, 1, 4, 9, 14},
	"1,2,1,2,1":                          {1, 1, 1, 2, 4},
	"1,2,1,3,1":                          {1, 1, 1, 2, 3},
	"0,2,1,2,1,5,2":                      {1, 1, 2, 5, 8},
	"1,2,1,2,1,2,1":                      {1, 1, 1, 2, 2},
	"-1,2,1,2,1,3,1,5,2,5,2":             {1, 2, 3, 5, 10},
	"0,3,1,3,1,6,1":                      {1, 1, 3, 5, 6},
	"0,3,1,3,1,5,1":                      {1, 1, 3, 5, 9},
	"0,3,1,3,1,4,1":                      {1, 1, 3, 8, 12},
	"-1,4,1,4,1,5,1,7,2":                 {1, 4, 5, 7, 16},
	"-1,2,1,2,1,2,1,3,1,4,1,5,2":         {1, 2, 3, 4, 5},
	"-1,3,1,4,1,5,2,7,3":                 {1, 3, 4, 5, 7},
	"0,2,1,2,1,2,1,2,1,3,1":              {1, 1, 2, 2, 3},
	"0,2,1,2,1,2,1,2,1,2,1":              {1, 1, 2, 2, 5},
	"-1,2,1,2,1,2,1,2,1,2,1,9,4":         {1, 2, 4, 5, 9},
	"-1,2,1,2,1,5,1,9,2":                 {1, 2, 5, 9, 16},
	"-1,2,1,3,1,3,1,3,1,3,1,7,1":         {1, 3, 6, 7, 8},
	"-1,2,1,2,1,2,1,11,3":                {1, 2, 3, 8, 11},
	"0,2,1,2,1,2,1,4,1":                  {1, 1, 2, 4, 7},
	"0,2,1,2,1,2,1,3,1":                  {1, 1, 2, 6, 9},
	"0,2,1,2,1,2,1,5,1":                  {1, 1, 2, 4, 5},
	"-1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,3,1": {1, 2, 2, 3, 7},
	"-1,2,1,8,3,11,5":                    {1, 5, 6, 8, 11},
	"-1,2,1,5,2,5,2,5,2":                 {1, 2, 3, 5, 5},
	"-1,2,1,3,1,3,1,9,2":                 {1, 2, 3, 7, 9},
	"-1,2,1,3,1,3,1,3,1,4,1,5,1":         {1, 3, 4, 5, 6},
	"0,3,1,4,1,4,1":                      {1, 1, 3, 4, 8},
	"0,2,1,3,1,3,1,3,1":                  {1, 1, 2, 3, 3},
	"0,3,1,4,1,5,1":                      {1, 1, 3, 4, 5},
	"-1,2,1,3,1,3,1,11,4":                {1, 3, 4, 11, 18},
	"-1,2,1,2,1,3,1,5,1,9,4":             {1, 4, 5, 6, 9},
	"-1,2,1,7,3,10,3":                    {1, 3, 4, 7, 10},
	"0,2,1,2,1,3,1,4,1":                  {1, 1, 2, 3, 4},
	"0,2,1,2,1,3,1,3,1":                  {1, 1, 2, 3, 6},
	"-1,2,1,2,1,5,2,8,3":                 {1, 2, 3, 5, 8},
	"-1,2,1,3,1,4,1,10,3":                {1, 3, 4, 10, 17},
	"-1,2,1,2,1,2,1,2,1,5,2,6,1":         {1, 2, 5, 6, 13},
	"-1,3,1,5,1,11,3":                    {1, 3, 5, 11, 19},
	"-1,2,1,2,1,2,1,2,1,3,1,7,3":         {1, 2, 3, 4, 7},
	"-1,2,1,2,1,2,1,2,1,2,1,2,1,5,2":     {1, 2, 2, 3, 5},
	"-1,2,1,2,1,2,1,3,1,3,1,3,1,3,1":     {1, 2, 3, 3, 4},
	"-1,2,1,2,1,2,1,6,1,7,2":             {1, 2, 5, 6, 7},
	"-1,3,1,3,1,5,1,8,3":                 {1, 3, 5, 16, 24},
	"-1,2,1,2,1,2,1,3,1,8,3":             {1, 2, 3, 8, 13},
	"-1,2,1,2,1,4,1,6,1,7,3":             {1, 4, 6, 7, 17},
	"0,2,1,5,1,6,1":                      {1, 1, 4, 5, 6},
	"0,2,1,5,1,5,1":                      {1, 1, 4, 5, 10},
	"-1,2,1,5,1,11,2":                    {1, 2, 5, 9, 11},
	"0,2,1,3,1,9,1":                      {1, 1, 6, 8, 9},
	"0,2,1,3,1,8,1":                      {1, 1, 6, 8, 15},
	"0,2,1,3,1,5,2":                      {1, 1, 2, 3, 5},
	"0,2,1,3,1,7,1":                      {1, 1, 6, 14, 21},
	"-1,3,1,3,1,3,1,3,1,3,1,4,1":         {1, 3, 3, 4, 5},
	"-1,2,1,2,1,3,1,3,1,7,2":             {1, 2, 3, 7, 12},
	"-1,2,1,3,1,7,3,7,3":                 {1, 3, 4, 7, 14},
	"-1,3,1,5,1,6,1,7,2":                 {1, 5, 6, 7, 9},
	"-1,4,1,5,2,5,2,7,1":                 {1, 5, 7, 8, 20},
	"-1,2,1,3,1,5,2,7,2":                 {1, 2, 3, 5, 7},
	"-1,2,1,3,1,5,2,11,5":                {1, 5, 6, 22, 33},
	"-1,2,1,5,1,6,1,8,3":                 {1, 5, 6, 8, 19},
	"-1,2,1,2,1,2,1,2,1,3,1,3,1,4,1":     {1, 2, 3, 4, 9},
	"-1,4,1,7,2,9,4":                     {1, 4, 5, 7, 9},
	"-1,2,1,2,1,2,1,2,1,2,1,3,1,7,1":     {1, 2, 6, 7, 15},
	"0,4,1,4,1,4,1":                      {1, 1, 3, 4, 4},
	"-1,3,1,4,1,7,2,8,1":                 {1, 7, 8, 9, 12},
	"-1,2,1,5,2,7,3,8,1":                 {1, 7, 8, 10, 25},
	"-1,2,1,2,1,3,1,4,1,5,1,5,1":         {1, 4, 5, 6, 15},
	"-1,2,1,2,1,6,1,11,4":                {1, 4, 6, 7, 11},
	"-1,2,1,2,1,2,1,3,1,3,1,5,2":         {1, 2, 3, 10, 15},
	"-1,2,1,2,1,2,1,2,1,2,1,4,1,5,1":     {1, 2, 4, 5, 11},
	"-1,3,1,3,1,4,1,4,1,5,2":             {1, 3, 4, 5, 12},
	"4":                                  {1, 1, 1, 1, 1, 1},
	"1,5,2":                              {1, 1, 1, 2, 3, 5},
	"1,5,1":                              {1, 1, 1, 3, 4, 5},
	"2,3,1":                              {1, 1, 1, 1, 2, 3},
	"3,2,1":                              {1, 1, 1, 1, 1, 2},
	"0,9,2":                              {1, 1, 2, 5, 7, 9},
	"0,11,4":                             {1, 1, 3, 4, 7, 11},
	"-1,2,1,2,1,3,1,4,1,4,1,7,1":         {1, 4, 6, 7, 8, 9},
	"0,5,2,5,2":                          {1, 1, 2, 3, 5, 5},
	"0,5,2,9,1":                          {1, 1, 5, 7, 8, 9},
	"-1,3,1,7,2,11,5":                    {1, 5, 6, 7, 9, 11},
	"-1,3,1,7,2,7,2":                     {1, 2, 3, 5, 7, 7},
	"-1,5,1,13,2":                        {1, 2, 5, 9, 11, 13},
	"0,5,1,7,3":                          {1, 1, 3, 4, 5, 7},
	"0,3,1,3,1,3,1,3,1":                  {1, 1, 2, 3, 3, 3},
	"-1,2,1,3,1,3,1,5,1,7,3":             {1, 3, 4, 5, 6, 7},
	"1,3,1,3,1":                          {1, 1, 1, 2, 3, 3},
	"0,3,1,9,4":                          {1, 1, 3, 4, 5, 9},
	"-1,2,1,2,1,2,1,5,2,7,3":             {1, 2, 3, 4, 5, 7},
	"0,3,1,7,2":                          {1, 1, 2, 3, 5, 7},
	"0,2,1,4,1,8,1":                      {1, 1, 4, 6, 7, 8},
	"0,2,1,4,1,5,2":                      {1, 1, 2, 3, 4, 5},
	"-1,2,1,2,1,2,1,2,1,2,1,5,1,5,1":     {1, 2, 4, 5, 5, 6},
	"0,2,1,8,3":                          {1, 1, 2, 3, 5, 8},
	"2,2,1,2,1":                          {1, 1, 1, 1, 2, 2},
	"1,2,1,4,1":                          {1, 1, 1, 2, 3, 4},
	"0,2,1,11,5":                         {1, 1, 4, 5, 6, 11},
	"-1,2,1,17,7":                        {1, 3, 4, 7, 10, 17},
	"0,2,1,2,1,7,3":                      {1, 1, 2, 3, 4, 7},
	"1,2,1,2,1,3,1":                      {1, 1, 1, 2, 2, 3},
	"-1,2,1,2,1,13,5":                    {1, 2, 3, 5, 8, 13},
	"0,3,1,3,1,7,1":                      {1, 1, 3, 5, 6, 7},
	"0,3,1,3,1,5,2":                      {1, 1, 2, 3, 3, 5},
	"-1,3,1,3,1,11,2":                    {1, 2, 3, 7, 9, 11},
	"-1,2,1,2,1,4,1,4,1,4,1,5,1":         {1, 4, 4, 5, 6, 7},
	"-2,2,1,2,1,2,1,2,1,2,1,2,1,2,1,3,1,3,1,5,2": {2, 3, 4, 5, 6, 7},
	"-1,2,1,2,1,4,1,5,2,5,2":                     {1, 2, 3, 4, 5, 5},
	"-1,2,1,2,1,2,1,2,1,10,3":                    {1, 2, 3, 4, 7, 10},
	"0,2,1,2,1,2,1,2,1,4,1":                      {1, 1, 2, 2, 3, 4},
	"-1,2,1,2,1,2,1,2,1,2,1,7,2":                 {1, 2, 2, 3, 5, 7},
	"0,2,1,2,1,2,1,2,1,2,1,2,1":                  {1, 1, 2, 2, 2, 3},
	"-1,4,1,5,2,10,3":                            {1, 3, 4, 5, 7, 10},
	"1,2,1,2,1,2,1,2,1":                          {1, 1, 1, 2, 2, 2},
	"0,2,1,2,1,2,1,5,2":                          {1, 1, 2, 2, 3, 5},
	"-1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1":     {1, 2, 2, 2, 3, 3},
	"0,2,1,2,1,2,1,6,1":                          {1, 1, 2, 4, 5, 6},
	"-1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,4,1":         {1, 2, 2, 3, 4, 5},
	"-1,5,2,5,2,7,2":                             {1, 2, 3, 5, 5, 7},
	"0,2,1,3,1,3,1,4,1":                          {1, 1, 2, 3, 3, 4},
	"0,3,1,4,1,6,1":                              {1, 1, 3, 4, 5, 6},
	"-1,3,1,3,1,3,1,3,1,7,3":                     {1, 3, 3, 4, 5, 7},
	"-1,2,1,3,1,3,1,3,1,9,4":                     {1, 3, 4, 5, 6, 9},
	"-1,2,1,2,1,3,1,3,1,3,1,5,2":                 {1, 2, 3, 3, 4, 5},
	"-1,2,1,3,1,3,1,4,1,4,1,4,1":                 {1, 3, 4, 4, 5, 6},
	"-1,4,1,7,3,8,3":                             {1, 3, 4, 5, 7, 8},
	"-1,3,1,3,1,6,1,8,3":                         {1, 3, 5, 6, 7, 8},
	"-1,2,1,2,1,3,1,14,5":                        {1, 4, 5, 6, 9, 14},
	"0,2,1,2,1,3,1,5,1":                          {1, 1, 2, 3, 4, 5},
	"-1,2,1,2,1,2,1,2,1,5,2,7,1":                 {1, 2, 5, 6, 7, 8},
	"0,3,1,5,1,5,1":                              {1, 1, 3, 4, 5, 5},
	"-1,2,1,2,1,2,1,2,1,2,1,2,1,3,1,3,1":         {1, 2, 2, 3, 3, 4},
	"-1,2,1,2,1,3,1,4,1,7,2":                     {1, 2, 3, 4, 5, 7},
	"-1,5,2,7,3,7,3":                             {1, 3, 4, 5, 7, 7},
	"-1,7,2,13,4":                                {1, 4, 5, 7, 9, 13},
	"-1,3,1,5,2,11,4":                            {1, 3, 4, 5, 7, 11},
	"-1,3,1,5,2,9,2":                             {1, 2, 3, 5, 7, 9},
	"0,2,1,2,1,2,1,3,1,3,1":                      {1, 1, 2, 2, 3, 3},
	"-1,2,1,2,1,5,1,5,1,7,3":                     {1, 4, 5, 6, 7, 10},
	"0,2,1,5,1,7,1":                              {1, 1, 4, 5, 6, 7},
	"0,2,1,6,1,6,1":                              {1, 1, 4, 5, 6, 6},
	"0,2,1,3,1,10,1":                             {1, 1, 6, 8, 9, 10},
	"-1,2,1,3,1,12,5":                            {1, 2, 3, 5, 7, 12},
	"-1,2,1,2,1,2,1,4,1,8,3":                     {1, 2, 3, 4, 5, 8},
	"-1,2,1,3,1,4,1,5,2,9,1":                     {1, 8, 9, 10, 12, 15},
	"-1,2,1,4,1,4,1,5,2,6,1":                     {1, 4, 5, 6, 7, 8},
	"-1,2,1,2,1,2,1,2,1,3,1,3,1,5,1":             {1, 2, 3, 4, 5, 6},
	"-1,2,1,2,1,2,1,2,1,2,1,3,1,8,1":             {1, 2, 6, 7, 8, 9},
	"-1,4,1,4,1,11,3":                            {1, 3, 4, 5, 8, 11},
	"0,4,1,4,1,5,1":                              {1, 1, 3, 4, 4, 5},
	"-1,2,1,2,1,2,1,2,1,3,1,4,1,4,1":             {1, 2, 3, 4, 4, 5},
	"-1,2,1,2,1,3,1,3,1,5,1,8,1":                 {1, 6, 8, 9, 10, 15},
	"0,2,1,2,1,4,1,4,1":                          {1, 1, 2, 3, 4, 4},
	"-1,2,1,7,2,8,3":                             {1, 2, 3, 5, 7, 8},
	"-1,2,1,2,1,6,1,9,2":                         {1, 2, 5, 6, 7, 9},
	"-1,2,1,2,1,2,1,2,1,2,1,4,1,6,1":             {1, 2, 4, 5, 6, 7},
	"5":                                          {1, 1, 1, 1, 1, 1, 1},
	"1,6,1":                                      {1, 1, 1, 3, 4, 5, 6},
	"2,4,1":                                      {1, 1, 1, 1, 2, 3, 4},
	"3,3,1":                                      {1, 1, 1, 1, 1, 2, 3},
	"4,2,1":                                      {1, 1, 1, 1, 1, 1, 2},
	"0,6,1,7,3":                                  {1, 1, 3, 4, 5, 6, 7},
	"-1,2,1,4,1,5,2,7,2":                         {1, 2, 3, 4, 5, 5, 7},
	"0,5,2,10,1":                                 {1, 1, 5, 7, 8, 9, 10},
	"-1,5,2,12,5":                                {1, 2, 3, 5, 5, 7, 12},
	"-1,3,1,3,1,3,1,10,3":                        {1, 3, 3, 4, 5, 7, 10},
	"0,3,1,3,1,3,1,4,1":                          {1, 1, 2, 3, 3, 3, 4},
	"0,4,1,7,2":                                  {1, 1, 2, 3, 4, 5, 7},
	"0,4,1,9,4":                                  {1, 1, 3, 4, 4, 5, 9},
	"-1,3,1,3,1,4,1,5,1,5,2":                     {1, 3, 4, 5, 5, 6, 7},
	"1,3,1,4,1":                                  {1, 1, 1, 2, 3, 3, 4},
	"0,3,1,8,3":                                  {1, 1, 2, 3, 3, 5, 8},
	"0,2,1,4,1,9,1":                              {1, 1, 4, 6, 7, 8, 9},
	"3,2,1,2,1":                                  {1, 1, 1, 1, 1, 2, 2},
	"2,2,1,3,1":                                  {1, 1, 1, 1, 2, 2, 3},
	"1,2,1,5,2":                                  {1, 1, 1, 2, 2, 3, 5},
	"1,2,1,5,1":                                  {1, 1, 1, 2, 3, 4, 5},
	"0,2,1,2,1,7,2":                              {1, 1, 2, 2, 3, 5, 7},
	"1,2,1,2,1,4,1":                              {1, 1, 1, 2, 2, 3, 4},
	"2,2,1,2,1,2,1":                              {1, 1, 1, 1, 2, 2, 2},
	"0,3,1,3,1,8,1":                              {1, 1, 3, 5, 6, 7, 8},
	"0,2,1,2,1,2,1,2,1,5,1":                      {1, 1, 2, 2, 3, 4, 5},
	"1,2,1,2,1,2,1,2,1,2,1":                      {1, 1, 1, 2, 2, 2, 2},
	"-1,2,1,2,1,2,1,2,1,9,2":                     {1, 2, 2, 3, 5, 7, 9},
	"0,2,1,2,1,2,1,2,1,2,1,3,1":                  {1, 1, 2, 2, 2, 3, 3},
	"1,2,1,2,1,2,1,3,1":                          {1, 1, 1, 2, 2, 2, 3},
	"0,2,1,2,1,2,1,7,1":                          {1, 1, 2, 4, 5, 6, 7},
	"0,2,1,3,1,3,1,5,1":                          {1, 1, 2, 3, 3, 4, 5},
	"0,3,1,4,1,7,1":                              {1, 1, 3, 4, 5, 6, 7},
	"0,3,1,4,1,5,2":                              {1, 1, 2, 3, 3, 4, 5},
	"-1,2,1,3,1,3,1,3,1,7,2":                     {1, 2, 3, 3, 4, 5, 7},
	"0,2,1,2,1,3,1,6,1":                          {1, 1, 2, 3, 4, 5, 6},
	"0,2,1,2,1,3,1,5,2":                          {1, 1, 2, 2, 3, 3, 5},
	"0,2,1,3,1,4,1,4,1":                          {1, 1, 2, 3, 3, 4, 4},
	"-1,2,1,3,1,4,1,9,2":                         {1, 2, 3, 4, 5, 7, 9},
	"-1,8,3,9,2":                                 {1, 2, 3, 5, 7, 8, 9},
	"-1,8,3,11,4":                                {1, 3, 4, 5, 7, 8, 11},
	"-1,2,1,2,1,7,2,7,3":                         {1, 2, 3, 4, 5, 7, 7},
	"0,3,1,5,1,6,1":                              {1, 1, 3, 4, 5, 5, 6},
	"-1,2,1,2,1,2,1,3,1,3,1,3,1,4,1":             {1, 2, 3, 3, 4, 4, 5},
	"-1,2,1,3,1,5,1,5,2,7,1":                     {1, 5, 6, 7, 8, 9, 10},
	"0,2,1,2,1,2,1,3,1,4,1":                      {1, 1, 2, 2, 3, 3, 4},
	"-1,2,1,2,1,2,1,7,1,7,2":                     {1, 2, 5, 6, 7, 7, 8},
	"-1,2,1,2,1,2,1,3,1,5,1,5,2":                 {1, 2, 3, 4, 5, 5, 6},
	"0,2,1,5,1,8,1":                              {1, 1, 4, 5, 6, 7, 8},
	"0,2,1,5,1,5,2":                              {1, 1, 2, 3, 4, 5, 5},
	"0,2,1,6,1,7,1":                              {1, 1, 4, 5, 6, 6, 7},
	"-1,2,1,6,1,11,2":                            {1, 2, 5, 6, 7, 9, 11},
	"0,2,1,3,1,7,3":                              {1, 1, 2, 3, 3, 4, 7},
	"1,2,1,3,1,3,1":                              {1, 1, 1, 2, 2, 3, 3},
	"0,2,1,3,1,11,1":                             {1, 1, 6, 8, 9, 10, 11},
	"-1,2,1,2,1,2,1,2,1,4,1,7,3":                 {1, 2, 3, 4, 4, 5, 7},
	"-1,2,1,2,1,2,1,4,1,4,1,5,2":                 {1, 2, 3, 4, 4, 5, 5},
	"-1,2,1,2,1,3,1,3,1,8,3":                     {1, 2, 3, 3, 4, 5, 8},
	"0,2,1,2,1,3,1,3,1,3,1":                      {1, 1, 2, 2, 3, 3, 3},
	"0,4,1,5,1,5,1":                              {1, 1, 3, 4, 4, 5, 5},
	"-1,3,1,6,1,11,3":                            {1, 3, 5, 6, 7, 8, 11},
	"-1,5,1,5,2,5,2,6,1":                         {1, 5, 5, 6, 7, 8, 9},
	"-1,2,1,2,1,2,1,2,1,2,1,3,1,5,2":             {1, 2, 2, 3, 3, 4, 5},
	"0,4,1,4,1,6,1":                              {1, 1, 3, 4, 4, 5, 6},
	"0,2,1,2,1,4,1,5,1":                          {1, 1, 2, 3, 4, 4, 5},
	"-1,2,1,3,1,3,1,5,2,5,2":                     {1, 2, 3, 3, 4, 5, 5},
	"-1,2,1,3,1,4,1,4,1,7,3":                     {1, 3, 4, 4, 5, 6, 7},
	"-1,2,1,4,1,5,1,5,1,5,2":                     {1, 4, 5, 5, 6, 7, 8},
	"-1,2,1,3,1,5,1,10,3":                        {1, 3, 4, 5, 6, 7, 10},
	"-1,2,1,2,1,4,1,4,1,9,4":                     {1, 4, 4, 5, 6, 7, 9},
}
