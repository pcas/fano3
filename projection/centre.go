// Centre TODO write descriptive comment here.

/*

Given centre p = 1/r(1,a,r-a) (assumed to have a <= r-a) on Fano X
and target Fano Y in P(a0,...,aN) which is the image of projection from p,
determine the (numerical) type/subtype of the projection and the
weights of the ambient of X in the case that the unprojection works
as suggested.

Case 1.
If 1, a, r-a are three distinct weights among the a0...aN, then
the projection is Type I and weights of X are a0...aN,r.

Case 2.
If a, r-a are two distinct weights among the a0...aN, and
n + 1 = min(a0..aN excluding a,r-a) then the projection is Type II_n
and weights of X are a0...aN,r,r+1,...,r+n.

Case 3.
If a = 2 and 1, r-a are two distinct weights among the a0...aN, and
2(n + 1) = min(a0..aN excluding 1,r-a) then the projection is Type II_n
and weights of X are a0...aN,r,r+2,...,r+2n.

Case 4.
Otherwise (in the current setup) no two of 1,a,r-a lie distinctly
among the weights of Y, and the projection is Type IV - and we have
no prediction of the weights of X.

*/

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package projection

import (
	"bitbucket.org/pcastools/slice"
	"sort"
)

// MinDivisibleInt returns the smallest value in the slice S divisible by k. If no such then 0 is returned.
func MinDivisibleInt(k int, S ...int) int {
	currentmin := 0
	for _, s := range S {
		if s%k == 0 {
			if currentmin == 0 || currentmin > s {
				currentmin = s
			}
		}
	}
	return currentmin
}

/*
// MinEvenInt returns the smallest even value in the slice S. If no such then 0 is returned.
func MinEvenInt(S ...int) int {
	if len(S) == 0 {
		return 0
	}
	// make a slice of the even elements of S
	T := make([]int, 0, len(S))
	for _, s := range S {
		if s%2 == 0 {
			T = append(T, s)
		}
	}
	return compare.MinInt(T...)
}
*/

// CentreAnalysis determines whether projection from p with target ambient
// weights WY is Type I, II_n or IV, and returns the ambient weights of the
// unprojection.
func CentreAnalysis(p []int, WY []int) (ptype int, subtype int, W []int) {
	ptype = 0
	subtype = 0
	WX := slice.CopyInt(WY)
	r := p[0]
	a := p[1]
	missing := 0
	num_matches := 0
	Wtmp := slice.CopyInt(WY)
	// Look for the biggest weight r-a in W
	idxra := slice.IndexInt(Wtmp, r-a)
	if idxra != -1 {
		Wtmp = slice.RemoveInt(Wtmp, idxra)
		num_matches++
	} else {
		missing = r - a
	}
	idxa := slice.IndexInt(Wtmp, a)
	if idxa != -1 {
		Wtmp = slice.RemoveInt(Wtmp, idxa)
		num_matches++
	} else {
		missing = a
	}
	// ... now the 1
	idx1 := slice.IndexInt(Wtmp, 1)
	if idx1 != -1 {
		Wtmp = slice.RemoveInt(Wtmp, idx1)
		num_matches++
	} else {
		missing = 1
	}
	if num_matches == 3 {
		ptype = 1
		WX = append(WX, r)
		sort.IntSlice(WX).Sort()
	} else if num_matches == 2 {
		ptype = 2
		WX = append(WX, r)
		n := MinDivisibleInt(missing, Wtmp...)
		if n == 0 {
			panic("No weights divisible by the missing divisor weight in target for Type II projection")
		}
		subtype = n/missing - 1
		for i := 1; i < n/missing; i++ {
			WX = append(WX, r+missing*i)
		}
		sort.IntSlice(WX).Sort()
	} else {
		ptype = 4
	}
	return ptype, subtype, WX
}
