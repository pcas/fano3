// Projection TODO need descriptive comment here.

/*

1.
Retrieve each grdbid, g, B, W, Pnum in turn.
Edit W, Pnum.
Update values.

2.
Retrieve each grdbid, g, B, W, Pnum in turn.
Compare with other values in sqlite.
Edit W, Pnum.
Update values.

*/

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package projection

import (
	"bitbucket.org/pcas/fano3/fanoutil"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial/integerpolynomial"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcastools/slice"
	"database/sql"
	"runtime"
	"sort"
	"sync"
)

// resData is the data defining a result
type fanoData struct {
	grdbid int
	g      int
	B      [][]int
	W      []*integer.Element
	Pnum   *integerpolynomial.Element
	deg    *rational.Element
}

type stcTriple struct {
	source int
	target int
	centre []int
}

type basket [][]int

//////////////////////////////////////////////////////////////////////

func (B basket) Swap(i int, j int) {
	B[i], B[j] = B[j], B[i]
}

func (B basket) Len() int {
	return len(B)
}

func (B basket) Less(i int, j int) bool {
	p1, p2 := B[i], B[j]
	return p1[0] < p2[0] || (p1[0] == p2[0] && p1[1] < p2[1])
}

//////////////////////////////////////////////////////////////////////

// Convert db row to a Fano.
func rowToFanoData(row *sql.Row) (*fanoData, error) {
	var g, id int
	var Bstr, Wstr, Pnumstr, degstr string
	if err := row.Scan(&id, &g, &Bstr, &Wstr, &Pnumstr, &degstr); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	// convert to math types
	// Pnum comes from sqlite as a sequence of all coeffs
	Pnumcs, err := integer.SliceFromString(Pnumstr)
	if err != nil {
		return nil, err
	}
	R := integerpolynomial.DefaultUnivariateRing()
	Pnum, err := integerpolynomial.FromIntegerSlice(R, 0, 1, Pnumcs)
	if err != nil {
		return nil, err
	}
	W, err := integer.SliceFromString(Wstr)
	if err != nil {
		return nil, err
	}
	deg, err := rational.FromString(degstr)
	if err != nil {
		return nil, err
	}
	B, err := fanoutil.SliceOfIntSliceFromString(Bstr)
	if err != nil {
		return nil, err
	}
	X := &fanoData{grdbid: id, g: g, B: B, W: W, Pnum: Pnum, deg: deg}
	return X, nil
}

// GetProjectionCentresAndTargetWeightsBySourceID retrieves that data from sqlite by its grdbid.
func GetProjectionCentresAndTargetWeightsBySourceID(id int, db *sql.DB, tablename string, projectiontablename string) (centres [][]int, targetWs [][]int, err error) {
	centres = make([][]int, 0, 5)
	targetWs = make([][]int, 0, 5)
	rows, err := db.Query("SELECT target,centrer,centrea FROM "+projectiontablename+" WHERE source = ?", id)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		var target int
		var r int
		var a int
		if err := rows.Scan(&target, &r, &a); err != nil {
			if err == sql.ErrNoRows {
				return nil, nil, nil
			}
			return nil, nil, err
		}
		// Now get hold of the target's weights.
		WY, err := GetFanoWeightsByID(target, db, tablename)
		if err != nil {
			panic(err)
		}
		centre := []int{r, a}
		centres = append(centres, centre)
		targetWs = append(targetWs, WY)
	}
	err = rows.Err()
	if err != nil {
		panic(err)
	}
	return centres, targetWs, nil
}

// GetFanoByID retrieves a Fano from sqlite by its grdbid.
func GetFanoByID(id int, db *sql.DB, tablename string) (*fanoData, error) {
	row := db.QueryRow("SELECT grdbid,g,B,W,Pnum,deg FROM "+tablename+" WHERE grdbid = ?", id)
	return rowToFanoData(row)
}

// GetFanoWeightsByID retrieves a Fano's ambient weights from sqlite by its grdbid.
func GetFanoWeightsByID(id int, db *sql.DB, tablename string) ([]int, error) {
	row := db.QueryRow("SELECT W FROM "+tablename+" WHERE grdbid = ?", id)
	var Wstr string
	if err := row.Scan(&Wstr); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	// convert strings to types and return
	W, err := slice.IntSliceFromString(Wstr)
	if err != nil {
		return nil, err
	}
	return W, nil
}

// GetFanoGenusBasketDegreeByID retrieves a Fano's genus, basket and degree from sqlite by its grdbid.
func GetFanoGenusBasketDegreeByID(id int, db *sql.DB, tablename string) (int, [][]int, *rational.Element, error) {
	row := db.QueryRow("SELECT g,B,deg FROM "+tablename+" WHERE grdbid = ?", id)
	var g int
	var Bstr string
	var degstr string
	if err := row.Scan(&g, &Bstr, &degstr); err != nil {
		if err == sql.ErrNoRows {
			return 0, nil, nil, nil
		}
		return 0, nil, nil, err
	}
	// convert strings to types and return
	deg, err := rational.FromString(degstr)
	if err != nil {
		return 0, nil, nil, err
	}
	B, err := fanoutil.SliceOfIntSliceFromString(Bstr)
	if err != nil {
		return 0, nil, nil, err
	}
	return g, B, deg, nil
}

// GetFanoByGenusBasket retrieves a Fano from sqlite by its g,B.
func GetFanoByGenusBasket(g int, B [][]int, db *sql.DB, tablename string) (*fanoData, error) {
	row := db.QueryRow("SELECT grdbid,g,B,W,Pnum,deg FROM "+tablename+" WHERE g = ? AND B = ?", g, fanoutil.SliceOfIntSliceToString(B))
	return rowToFanoData(row)
}

// GetFanoIndexByGenusBasket retrieves the index of a Fano from sqlite by its g,B.
func GetFanoIndexByGenusBasket(g int, B [][]int, db *sql.DB, tablename string) (int, error) {
	row := db.QueryRow("SELECT grdbid FROM "+tablename+" WHERE g = ? AND B = ?", g, fanoutil.SliceOfIntSliceToString(B))
	var grdbid int
	if err := row.Scan(&grdbid); err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}
		return 0, err
	}
	return grdbid, nil
}

// GetRestrictedFanoIndexByGenusBasket retrieves the index of a Fano from
// sqlite by its g,B BUT restricting to those indices < i.
func GetRestrictedFanoIndexByGenusBasket(g int, B [][]int, db *sql.DB, tablename string, i int) (int, error) {
	row := db.QueryRow("SELECT grdbid FROM "+tablename+" WHERE g = ? AND B = ? AND grdbid < ?", g, fanoutil.SliceOfIntSliceToString(B), i)
	var grdbid int
	if err := row.Scan(&grdbid); err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}
		return 0, err
	}
	return grdbid, nil
}

// newSing does: given [r,a] return [[r-a,-r], [a,-r]] (normalising as a basket)
func newSing(p []int) [][]int {
	r := p[0]
	if r == 2 {
		return [][]int{}
	}
	a := p[1]
	if a == 1 {
		return [][]int{{r - 1, 1}}
	}
	r1 := r - a
	a1 := (-r % r1) + r1
	if 2*a1 > r1 {
		a1 = r1 - a1
	}
	r2 := a
	a2 := (-r % r2) + r2
	if 2*a2 > r2 {
		a2 = r2 - a2
	}
	return [][]int{{r1, a1}, {r2, a2}}
}

// Worker to analyse projection from [r,a] (for an index 1 Fano 3-fold)
func checkProjection(B [][]int, deg *rational.Element, i int) [][]int {
	p := B[i]
	r := p[0]
	a := p[1]
	correction, _ := rational.FromIntPair(1, (r * a * (r - a)))
	targ_deg := rational.Subtract(deg, correction)
	if !targ_deg.IsPositive() {
		return nil
	}
	// New basket is [r-a,-r], [a, -r], with right modulus choices
	new_sing := newSing(p)
	newB := make([][]int, 0, len(B)+1)
	// first remove the i-th entry and append the projected singularities
	newB = append(newB, B[0:i]...)
	newB = append(newB, B[i+1:len(B)]...)
	newB = append(newB, new_sing...)
	sort.Sort(basket(newB))
	return newB
}

// reducedBasketIndices returns a slice of the indices of the reduced basket
// that contains a single sing from the input basket B.
// This assumes that the basket B is sorted.
func reducedBasketIndices(B [][]int) []int {
	if len(B) == 0 {
		return nil
	}
	Binds := make([]int, 0, len(B))
	bcurr := B[0]
	Binds = append(Binds, 0)
	for i, b := range B[1:] {
		if b[0] != bcurr[0] || b[1] != bcurr[1] {
			bcurr = b
			Binds = append(Binds, i+1)
		}
	}
	return Binds
}

// projectedBaskets takes the basket B and degree deg of a Fano 3-fold and
// returns a pair of parallel sequences: the first lists the singularities in
// the basket (each only once), the second lists the baskets after projection
// from that corresponding singularity.
func projectedBaskets(B [][]int, deg *rational.Element) ([]int, [][][]int) {
	target_Bs := make([][][]int, 0, len(B))
	Binds := reducedBasketIndices(B)
	for _, idx := range Binds {
		target_B := checkProjection(B, deg, idx)
		target_Bs = append(target_Bs, target_B)
	}
	return Binds, target_Bs
}

// worker figures out the stc (source/target/centre) triples of projection for
// each Fano id that comes into the idC channel.
func worker(db *sql.DB, m *sync.RWMutex, tablename string, idC <-chan int, resC chan<- *stcTriple, doneC chan<- bool) {
	for id := range idC {
		// retrieve basket and degree of Fano id
		m.RLock()
		g, B, deg, err := GetFanoGenusBasketDegreeByID(id, db, tablename)
		m.RUnlock()
		if err != nil {
			panic(err)
		}
		// find the projected baskets
		Binds, target_Bs := projectedBaskets(B, deg)
		// send stc triple onto results channel
		for idx, pidx := range Binds {
			Btarg := target_Bs[idx]
			// recover projected ids
			m.RLock()
			target_id, err := GetFanoIndexByGenusBasket(g, Btarg, db, tablename)
			m.RUnlock()
			if err != nil {
				panic(err)
			}
			if target_id != 0 {
				stc := &stcTriple{source: id, target: target_id, centre: B[pidx]}
				resC <- stc
			}
		}
	}
	doneC <- true
}

// createProjectionTable creates the table which holds projection data.
func createProjectionTable(db *sql.DB, projectiontablename string) {
	// Delete the table if it already exists
	db.Exec("DROP TABLE IF EXISTS " + projectiontablename)
	// Create the table
	createsql := "CREATE TABLE " + projectiontablename + "(source INT, target INT, centrer INT, centrea INT, centrestr STRING, type INT, subtype INT)"
	if _, err := db.Exec(createsql); err != nil {
		panic(err)
	}
	if _, err := db.Exec("CREATE INDEX source_index ON " + projectiontablename + "(source ASC)"); err != nil {
		panic(err)
	}
	if _, err := db.Exec("CREATE INDEX target_index ON " + projectiontablename + "(target ASC)"); err != nil {
		panic(err)
	}
	if _, err := db.Exec("CREATE INDEX centrer_index ON " + projectiontablename + "(centrer ASC)"); err != nil {
		panic(err)
	}
}

// outputProjectionResults writes a source-target-centre triple to the
// projection table.
func outputProjectionResults(db *sql.DB, m *sync.RWMutex, projectiontablename string, resC <-chan *stcTriple, doneC chan<- struct{}) {
	defer close(doneC)

	// Prepare the INSERT statement
	createsql := "INSERT INTO " + projectiontablename + "(source,target,centrer,centrea,centrestr) VALUES (?,?,?,?,?)"
	stmt, err := db.Prepare(createsql)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	// Loop over the results and prepare their data; then add to db
	for STC := range resC {
		m.Lock()
		_, err := stmt.Exec(STC.source, STC.target, STC.centre[0], STC.centre[1], fanoutil.IntegerSliceToSingularity(STC.centre))
		m.Unlock()
		if err != nil {
			panic(err)
		}
	}
}

// BuildProjectionTable writes the initial projection data to sqlite
func BuildProjectionTable(tablename string, projectiontablename string) {
	// Create the communication channels
	idC := make(chan int)
	resC := make(chan *stcTriple, 10000)
	workerDoneC := make(chan bool)
	outputDoneC := make(chan struct{})

	// Create a read-write mutex to coordiate database access
	m := &sync.RWMutex{}

	// Open a connection to the SQL database
	db, err := sql.Open("sqlite3", "fano3output.db")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// Create the table
	// in sqlite3
	createProjectionTable(db, projectiontablename)

	// Start the output writer
	go outputProjectionResults(db, m, projectiontablename, resC, outputDoneC)

	//////////////////////////////////////////////////
	// Start the workers
	numWorkers := runtime.NumCPU() + 1
	for i := 0; i < numWorkers; i++ {
		go worker(db, m, tablename, idC, resC, workerDoneC)
	}

	number_of_fanos := 39550 // THINK: get from db
	for i := 1; i <= number_of_fanos; i++ {
		idC <- i
	}
	close(idC)
	for i := 0; i < numWorkers; i++ {
		<-workerDoneC
	}
	close(resC)
	<-outputDoneC
}
