// Singularity creates singularities for Fano 3-folds for use in the fano3 database.

/*

Create singularities for Fano 3-folds for use in the fano3 database.
These are NOT generic sing/basket functions.

Singularities are 1/r(1,a,r-a) represented as [r,a] where a <= r/2
and hcf(r,a) = 1.

*/

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package singularity

import (
	"bitbucket.org/pcas/fano3/fanoutil"
	"bitbucket.org/pcastools/mathutil"
	"database/sql/driver"
	"errors"
	"strconv"
	"strings"
)

// Point represents a singularity.
type Point struct {
	r int
	a int
}

// PointSlice
type PointSlice []*Point

//////////////////////////////////////////////////////////////////////

// New makes the singularity 1/r(1,a,r-a), replacing a by its least residue mod r, and switching a and r-a if necessary so a is the smaller of the two.
func New(r int, a int) (*Point, error) {
	if r <= 0 {
		return nil, errors.New("Index r must be positive")
	} else if gcd, _, _ := mathutil.XGCDOfPairInt64(int64(r), int64(a)); gcd != 1 {
		return nil, errors.New("r and a must be coprime")
	}
	a = a % r
	if a < 0 {
		a += r
	}
	if 2*a > r {
		a = r - a
	}
	return &Point{
		r: r,
		a: a,
	}, nil
}

// FromString converts strings like "[r,a]" to a singularity
func FromString(s string) (*Point, error) {
	s = strings.TrimSpace(s)
	if len(s) < 5 || (s[0] != '[' && s[len(s)] != ']') {
		return nil, errors.New("String is not a singularity [r,a]")
	}
	s = s[1 : len(s)-1]
	sstr := strings.Split(s, ",")
	if len(sstr) != 2 {
		return nil, errors.New("String is not a singularity [r,a]")
	}
	r, err := strconv.Atoi(strings.TrimSpace(sstr[0]))
	if err != nil {
		return nil, errors.New("String is not a singularity [r,a]")
	}
	a, err := strconv.Atoi(strings.TrimSpace(sstr[1]))
	if err != nil {
		return nil, errors.New("String is not a singularity [r,a]")
	}
	return New(r, a)
}

// String returns a point slice as a string
func (b PointSlice) String() string {
	S := make([]string, 0, b.Len())
	for _, p := range b {
		S = append(S, p.String())
	}
	return "[" + strings.Join(S, ",") + "]"
}

// PointSliceFromString returns a point slice
func PointSliceFromString(s string) (PointSlice, error) {
	ss, err := fanoutil.SliceOfIntSliceFromString(s)
	if err != nil {
		return nil, err
	}
	b := make(PointSlice, 0, len(ss))
	for _, x := range ss {
		if len(x) != 2 {
			return nil, errors.New("String does not represent basket")
		}
		p, err := New(x[0], x[1])
		if err != nil {
			return nil, err
		}
		b = append(b, p)
	}
	return b, nil
}

// Index returns the index of the singularity
func (p *Point) Index() int {
	if p == nil {
		return 1
	}
	return p.r
}

// Polarisation returns the polarisation of the singularity
func (p *Point) Polarisation() int {
	if p == nil {
		return 0
	}
	return p.a
}

// String represents a singularity as a string (as an int slice)
func (p *Point) String() string {
	return "[" + strconv.Itoa(p.Index()) + "," + strconv.Itoa(p.Polarisation()) + "]"
}

// AreEqual returns true iff the two arguments are the same singularity.
func AreEqual(p *Point, q *Point) bool {
	return p.Index() == q.Index() && p.Polarisation() == q.Polarisation()
}

//////////////////////////////////////////////////////////////////////
// Functions to enable sorting of PointSlices

// Less returns true iff p = [r,a] < q = [s,b] in the sense r<s or = and a<b
func (p *Point) Less(q *Point) bool {
	return p.Index() < q.Index() || (p.Index() == q.Index() && p.Polarisation() < q.Polarisation())
}

// Swap exchanges ith and jth entries of PointSlice B
func (B PointSlice) Swap(i int, j int) {
	B[i], B[j] = B[j], B[i]
}

// Len returns the length of the PointSlice B
func (B PointSlice) Len() int {
	return len(B)
}

// Less returns true iff the ith point of B is less than the jth point of B (in the sense of singularities)
func (B PointSlice) Less(i int, j int) bool {
	return B[i].Less(B[j])
}

//////////////////////////////////////////////////////////////////////
// Functions to assist database translation

// Value
func (p *Point) Value() (driver.Value, error) {
	return p.String(), nil
}

// Scan
func (p *Point) Scan(src interface{}) error {
	if p == nil {
		return errors.New("Illegal nil argument")
	}
	s, ok := src.(string)
	if !ok {
		return errors.New("Scan only accepts a string")
	}
	q, err := FromString(s)
	if err != nil {
		return err
	}
	p.r = q.Index()
	p.a = q.Polarisation()
	return nil
}
