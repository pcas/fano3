// Singularity_test provides tests for the singularity package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package singularity

import (
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

func TestIntSlice(t *testing.T) {
	assert := assert.New(t)
	p1, _ := New(7, 3)
	p2, _ := New(7, -11)
	assert.Equal(p1.Index(), p2.Index())
	assert.Equal(p1.Polarisation(), p2.Polarisation())
	assert.True(AreEqual(p1, p2))
	str := p1.String()
	assert.Equal("[7,3]", str)
	p3, _ := FromString("   [ 7   , -18 ]  ")
	assert.True(AreEqual(p1, p3))
}
